       # Readable pylint messages 
       #### C0102 - Black listed name "%s" 
       # Used when the name is listed in the black list (unauthorized names).

       # Black listed name "%s" 
       # blacklisted-name,

       #### C0103 - Invalid %s name "%s"%s 
       # Used when the name doesn't match the regular expression associated to
       # its type (constant, variable, class...).

       # Invalid %s name "%s"%s 
       # invalid-name,

       #### C0111 - Missing %s docstring 
       # Used when a module, function, class or method has no docstring.Some
       # special methods like __init__ doesn't necessary require a docstring.

       # Missing %s docstring 
       # missing-docstring,

       #### C0112 - Empty %s docstring 
       # Used when a module, function, class or method has an empty docstring
       # (it would be too easy ;).

       # Empty %s docstring 
       # empty-docstring,

       #### C0113 - Consider changing "%s" to "%s" 
       # Used when a boolean expression contains an unneeded negation.

       # Consider changing "%s" to "%s" 
       # unneeded-not,

       #### C0121 - Comparison to %s should be %s 
       # Used when an expression is compared to singleton values like True,
       # False or None.

       # Comparison to %s should be %s 
       # singleton-comparison,

       #### C0122 - Comparison should be %s 
       # Used when the constant is placed on the left side of a comparison. It
       # is usually clearer in intent to place it in the right hand side of the
       # comparison.

       # Comparison should be %s 
       # misplaced-comparison-constant,

       #### C0123 - Using type() instead of isinstance() for a typecheck. 
       # The idiomatic way to perform an explicit typecheck in Python is to use
       # isinstance(x, Y) rather than type(x) == Y, type(x) is Y. Though there
       # are unusual situations where these give different results.

       # Using type() instead of isinstance() for a typecheck. 
       # unidiomatic-typecheck,

       #### C0200 - Consider using enumerate instead of iterating with range and len 
       # Emitted when code that iterates with range and len is encountered.
       # Such code can be simplified by using the enumerate builtin.

       # Consider using enumerate instead of iterating with range and len 
       # consider-using-enumerate,

       #### C0201 - Consider iterating the dictionary directly instead of calling .keys() 
       # Emitted when the keys of a dictionary are iterated through the .keys()
       # method. It is enough to just iterate through the dictionary itself, as
       # in "for key in dictionary".

       # Consider iterating the dictionary directly instead of calling .keys() 
       # consider-iterating-dictionary,

       #### C0202 - Class method %s should have %s as first argument 
       # Used when a class method has a first argument named differently than
       # the value specified in valid-classmethod-first-arg option (default to
       # "cls"), recommended to easily differentiate them from regular instance
       # methods.

       # Class method %s should have %s as first argument 
       # bad-classmethod-argument,

       #### C0203 - Metaclass method %s should have %s as first argument 
       # Used when a metaclass method has a first argument named differently
       # than the value specified in valid-classmethod-first-arg option
       # (default to "cls"), recommended to easily differentiate them from
       # regular instance methods.

       # Metaclass method %s should have %s as first argument 
       # bad-mcs-method-argument,

       #### C0204 - Metaclass class method %s should have %s as first argument 
       # Used when a metaclass class method has a first argument named
       # differently than the value specified in valid-metaclass-classmethod-
       # first-arg option (default to "mcs"), recommended to easily
       # differentiate them from regular instance methods.

       # Metaclass class method %s should have %s as first argument 
       # bad-mcs-classmethod-argument,

       #### C0205 - Class __slots__ should be a non-string iterable 
       # Used when a class __slots__ is a simple string, rather than an
       # iterable.

       # Class __slots__ should be a non-string iterable 
       # single-string-used-for-slots,

       #### C0301 - Line too long (%s/%s) 
       # Used when a line is longer than a given number of characters.

       # Line too long (%s/%s) 
       # line-too-long,

       #### C0302 - Too many lines in module (%s/%s) 
       # Used when a module has too much lines, reducing its readability.

       # Too many lines in module (%s/%s) 
       # too-many-lines,

       #### C0303 - Trailing whitespace 
       # Used when there is whitespace between the end of a line and the
       # newline.

       # Trailing whitespace 
       # trailing-whitespace,

       #### C0304 - Final newline missing 
       # Used when the last line in a file is missing a newline.

       # Final newline missing 
       # missing-final-newline,

       #### C0305 - Trailing newlines 
       # Used when there are trailing blank lines in a file.

       # Trailing newlines 
       # trailing-newlines,

       #### C0321 - More than one statement on a single line 
       # Used when more than on statement are found on the same line.

       # More than one statement on a single line 
       # multiple-statements,

       #### C0325 - Unnecessary parens after %r keyword 
       # Used when a single item in parentheses follows an if, for, or other
       # keyword.

       # Unnecessary parens after %r keyword 
       # superfluous-parens,

       #### C0326 - %s space %s %s %s 
       # Used when a wrong number of spaces is used around an operator, bracket
       # or block opener.

       # %s space %s %s %s 
       # bad-whitespace,

       #### C0327 - Mixed line endings LF and CRLF 
       # Used when there are mixed (LF and CRLF) newline signs in a file.

       # Mixed line endings LF and CRLF 
       # mixed-line-endings,

       #### C0328 - Unexpected line ending format. There is '%s' while it should be '%s'. 
       # Used when there is different newline than expected.

       # Unexpected line ending format. There is '%s' while it should be '%s'. 
       # unexpected-line-ending-format,

       #### C0330 - Wrong %s indentation%s%s. 
       # TODO

       # Wrong %s indentation%s%s. 
       # bad-continuation,

       #### C0401 - Wrong spelling of a word '%s' in a comment: 
       # Used when a word in comment is not spelled correctly.

       # Wrong spelling of a word '%s' in a comment: 
       # wrong-spelling-in-comment,

       #### C0402 - Wrong spelling of a word '%s' in a docstring: 
       # Used when a word in docstring is not spelled correctly.

       # Wrong spelling of a word '%s' in a docstring: 
       # wrong-spelling-in-docstring,

       #### C0403 - Invalid characters %r in a docstring 
       # Used when a word in docstring cannot be checked by enchant.

       # Invalid characters %r in a docstring 
       # invalid-characters-in-docstring,

       #### C0410 - Multiple imports on one line (%s) 
       # Used when import statement importing multiple modules is detected.

       # Multiple imports on one line (%s) 
       # multiple-imports,

       #### C0411 - %s should be placed before %s 
       # Used when PEP8 import order is not respected (standard imports first,
       # then third-party libraries, then local imports)

       # %s should be placed before %s 
       # wrong-import-order,

       #### C0412 - Imports from package %s are not grouped 
       # Used when imports are not grouped by packages

       # Imports from package %s are not grouped 
       # ungrouped-imports,

       #### C0413 - Import "%s" should be placed at the top of the module 
       # Used when code and imports are mixed

       # Import "%s" should be placed at the top of the module 
       # wrong-import-position,

       #### C1801 - Do not use `len(SEQUENCE)` as condition value 
       # Used when Pylint detects incorrect use of len(sequence) inside
       # conditions.

       # Do not use `len(SEQUENCE)` as condition value 
       # len-as-condition,

       #### E0001 -  
       # Used when a syntax error is raised for a module.

       #  
       # syntax-error,

       #### E0011 - Unrecognized file option %r 
       # Used when an unknown inline option is encountered.

       # Unrecognized file option %r 
       # unrecognized-inline-option,

       #### E0012 - Bad option value %r 
       # Used when a bad value for an inline option is encountered.

       # Bad option value %r 
       # bad-option-value,

       #### E0100 - __init__ method is a generator 
       # Used when the special class method __init__ is turned into a generator
       # by a yield in its body.

       # __init__ method is a generator 
       # init-is-generator,

       #### E0101 - Explicit return in __init__ 
       # Used when the special class method __init__ has an explicit return
       # value.

       # Explicit return in __init__ 
       # return-in-init,

       #### E0102 - %s already defined line %s 
       # Used when a function / class / method is redefined.

       # %s already defined line %s 
       # function-redefined,

       #### E0103 - %r not properly in loop 
       # Used when break or continue keywords are used outside a loop.

       # %r not properly in loop 
       # not-in-loop,

       #### E0104 - Return outside function 
       # Used when a "return" statement is found outside a function or method.

       # Return outside function 
       # return-outside-function,

       #### E0105 - Yield outside function 
       # Used when a "yield" statement is found outside a function or method.

       # Yield outside function 
       # yield-outside-function,

       #### E0107 - Use of the non-existent %s operator 
       # Used when you attempt to use the C-style pre-increment orpre-decrement
       # operator -- and ++, which doesn't exist in Python.

       # Use of the non-existent %s operator 
       # nonexistent-operator,

       #### E0108 - Duplicate argument name %s in function definition 
       # Duplicate argument names in function definitions are syntax errors.

       # Duplicate argument name %s in function definition 
       # duplicate-argument-name,

       #### E0110 - Abstract class %r with abstract methods instantiated 
       # Used when an abstract class with `abc.ABCMeta` as metaclass has
       # abstract methods and is instantiated.

       # Abstract class %r with abstract methods instantiated 
       # abstract-class-instantiated,

       #### E0111 - The first reversed() argument is not a sequence 
       # Used when the first argument to reversed() builtin isn't a sequence
       # (does not implement __reversed__, nor __getitem__ and __len__

       # The first reversed() argument is not a sequence 
       # bad-reversed-sequence,

       #### E0112 - More than one starred expression in assignment 
       # Emitted when there are more than one starred expressions (`*x`) in an
       # assignment. This is a SyntaxError. This message can't be emitted when
       # using Python < 3.0.

       # More than one starred expression in assignment 
       # too-many-star-expressions,

       #### E0113 - Starred assignment target must be in a list or tuple 
       # Emitted when a star expression is used as a starred assignment target.
       # This message can't be emitted when using Python < 3.0.

       # Starred assignment target must be in a list or tuple 
       # invalid-star-assignment-target,

       #### E0114 - Can use starred expression only in assignment target 
       # Emitted when a star expression is not used in an assignment target.
       # This message can't be emitted when using Python < 3.0.

       # Can use starred expression only in assignment target 
       # star-needs-assignment-target,

       #### E0115 - Name %r is nonlocal and global 
       # Emitted when a name is both nonlocal and global. This message can't be
       # emitted when using Python < 3.0.

       # Name %r is nonlocal and global 
       # nonlocal-and-global,

       #### E0116 - 'continue' not supported inside 'finally' clause 
       # Emitted when the `continue` keyword is found inside a finally clause,
       # which is a SyntaxError.

       # 'continue' not supported inside 'finally' clause 
       # continue-in-finally,

       #### E0117 - nonlocal name %s found without binding 
       # Emitted when a nonlocal variable does not have an attached name
       # somewhere in the parent scopes This message can't be emitted when
       # using Python < 3.0.

       # nonlocal name %s found without binding 
       # nonlocal-without-binding,

       #### E0118 - Name %r is used prior to global declaration 
       # Emitted when a name is used prior a global declaration, which results
       # in an error since Python 3.6. This message can't be emitted when using
       # Python < 3.6.

       # Name %r is used prior to global declaration 
       # used-prior-global-declaration,

       #### E0202 - An attribute defined in %s line %s hides this method 
       # Used when a class defines a method which is hidden by an instance
       # attribute from an ancestor class or set by some client code.

       # An attribute defined in %s line %s hides this method 
       # method-hidden,

       #### E0203 - Access to member %r before its definition line %s 
       # Used when an instance member is accessed before it's actually
       # assigned.

       # Access to member %r before its definition line %s 
       # access-member-before-definition,

       #### E0211 - Method has no argument 
       # Used when a method which should have the bound instance as first
       # argument has no argument defined.

       # Method has no argument 
       # no-method-argument,

       #### E0213 - Method should have "self" as first argument 
       # Used when a method has an attribute different the "self" as first
       # argument. This is considered as an error since this is a so common
       # convention that you shouldn't break it!

       # Method should have "self" as first argument 
       # no-self-argument,

       #### E0236 - Invalid object %r in __slots__, must contain only non empty strings 
       # Used when an invalid (non-string) object occurs in __slots__.

       # Invalid object %r in __slots__, must contain only non empty strings 
       # invalid-slots-object,

       #### E0237 - Assigning to attribute %r not defined in class slots 
       # Used when assigning to an attribute not defined in the class slots.

       # Assigning to attribute %r not defined in class slots 
       # assigning-non-slot,

       #### E0238 - Invalid __slots__ object 
       # Used when an invalid __slots__ is found in class. Only a string, an
       # iterable or a sequence is permitted.

       # Invalid __slots__ object 
       # invalid-slots,

       #### E0239 - Inheriting %r, which is not a class. 
       # Used when a class inherits from something which is not a class.

       # Inheriting %r, which is not a class. 
       # inherit-non-class,

       #### E0240 - Inconsistent method resolution order for class %r 
       # Used when a class has an inconsistent method resolution order.

       # Inconsistent method resolution order for class %r 
       # inconsistent-mro,

       #### E0241 - Duplicate bases for class %r 
       # Used when a class has duplicate bases.

       # Duplicate bases for class %r 
       # duplicate-bases,

       #### E0301 - __iter__ returns non-iterator 
       # Used when an __iter__ method returns something which is not an
       # iterable (i.e. has no `__next__` method)

       # __iter__ returns non-iterator 
       # non-iterator-returned,

       #### E0302 - The special method %r expects %s param(s), %d %s given 
       # Emitted when a special method was defined with an invalid number of
       # parameters. If it has too few or too many, it might not work at all.

       # The special method %r expects %s param(s), %d %s given 
       # unexpected-special-method-signature,

       #### E0303 - __len__ does not return non-negative integer 
       # Used when an __len__ method returns something which is not a non-
       # negative integer

       # __len__ does not return non-negative integer 
       # invalid-length-returned,

       #### E0401 - Unable to import %s 
       # Used when pylint has been unable to import a module.

       # Unable to import %s 
       # import-error,

       #### E0402 - Attempted relative import beyond top-level package 
       # Used when a relative import tries to access too many levels in the
       # current package.

       # Attempted relative import beyond top-level package 
       # relative-beyond-top-level,

       #### E0601 - Using variable %r before assignment 
       # Used when a local variable is accessed before it's assignment.

       # Using variable %r before assignment 
       # used-before-assignment,

       #### E0602 - Undefined variable %r 
       # Used when an undefined variable is accessed.

       # Undefined variable %r 
       # undefined-variable,

       #### E0603 - Undefined variable name %r in __all__ 
       # Used when an undefined variable name is referenced in __all__.

       # Undefined variable name %r in __all__ 
       # undefined-all-variable,

       #### E0604 - Invalid object %r in __all__, must contain only strings 
       # Used when an invalid (non-string) object occurs in __all__.

       # Invalid object %r in __all__, must contain only strings 
       # invalid-all-object,

       #### E0611 - No name %r in module %r 
       # Used when a name cannot be found in a module.

       # No name %r in module %r 
       # no-name-in-module,

       #### E0632 - Possible unbalanced tuple unpacking with sequence%s: left side has %d label(s), right side has %d value(s) 
       # Used when there is an unbalanced tuple unpacking in assignment

       # Possible unbalanced tuple unpacking with sequence%s: left side has %d label(s), right side has %d value(s) 
       # unbalanced-tuple-unpacking,

       #### E0633 - Attempting to unpack a non-sequence%s 
       # Used when something which is not a sequence is used in an unpack
       # assignment

       # Attempting to unpack a non-sequence%s 
       # unpacking-non-sequence,

       #### E0701 - Bad except clauses order (%s) 
       # Used when except clauses are not in the correct order (from the more
       # specific to the more generic). If you don't fix the order, some
       # exceptions may not be caught by the most specific handler.

       # Bad except clauses order (%s) 
       # bad-except-order,

       #### E0702 - Raising %s while only classes or instances are allowed 
       # Used when something which is neither a class, an instance or a string
       # is raised (i.e. a `TypeError` will be raised).

       # Raising %s while only classes or instances are allowed 
       # raising-bad-type,

       #### E0703 - Exception context set to something which is not an exception, nor None 
       # Used when using the syntax "raise ... from ...", where the exception
       # context is not an exception, nor None. This message can't be emitted
       # when using Python < 3.0.

       # Exception context set to something which is not an exception, nor None 
       # bad-exception-context,

       #### E0704 - The raise statement is not inside an except clause 
       # Used when a bare raise is not used inside an except clause. This
       # generates an error, since there are no active exceptions to be
       # reraised. An exception to this rule is represented by a bare raise
       # inside a finally clause, which might work, as long as an exception is
       # raised inside the try block, but it is nevertheless a code smell that
       # must not be relied upon.

       # The raise statement is not inside an except clause 
       # misplaced-bare-raise,

       #### E0710 - Raising a new style class which doesn't inherit from BaseException 
       # Used when a new style class which doesn't inherit from BaseException
       # is raised.

       # Raising a new style class which doesn't inherit from BaseException 
       # raising-non-exception,

       #### E0711 - NotImplemented raised - should raise NotImplementedError 
       # Used when NotImplemented is raised instead of NotImplementedError

       # NotImplemented raised - should raise NotImplementedError 
       # notimplemented-raised,

       #### E0712 - Catching an exception which doesn't inherit from Exception: %s 
       # Used when a class which doesn't inherit from Exception is used as an
       # exception in an except clause.

       # Catching an exception which doesn't inherit from Exception: %s 
       # catching-non-exception,

       #### E1003 - Bad first argument %r given to super() 
       # Used when another argument than the current class is given as first
       # argument of the super builtin.

       # Bad first argument %r given to super() 
       # bad-super-call,

       #### E1101 - %s %r has no %r member%s 
       # Used when a variable is accessed for an unexistent member.

       # %s %r has no %r member%s 
       # no-member,

       #### E1102 - %s is not callable 
       # Used when an object being called has been inferred to a non callable
       # object

       # %s is not callable 
       # not-callable,

       #### E1111 - Assigning to function call which doesn't return 
       # Used when an assignment is done on a function call but the inferred
       # function doesn't return anything.

       # Assigning to function call which doesn't return 
       # assignment-from-no-return,

       #### E1120 - No value for argument %s in %s call 
       # Used when a function call passes too few arguments.

       # No value for argument %s in %s call 
       # no-value-for-parameter,

       #### E1121 - Too many positional arguments for %s call 
       # Used when a function call passes too many positional arguments.

       # Too many positional arguments for %s call 
       # too-many-function-args,

       #### E1123 - Unexpected keyword argument %r in %s call 
       # Used when a function call passes a keyword argument that doesn't
       # correspond to one of the function's parameter names.

       # Unexpected keyword argument %r in %s call 
       # unexpected-keyword-arg,

       #### E1124 - Argument %r passed by position and keyword in %s call 
       # Used when a function call would result in assigning multiple values to
       # a function parameter, one value from a positional argument and one
       # from a keyword argument.

       # Argument %r passed by position and keyword in %s call 
       # redundant-keyword-arg,

       #### E1125 - Missing mandatory keyword argument %r in %s call 
       # Used when a function call does not pass a mandatory keyword-only
       # argument. This message can't be emitted when using Python < 3.0.

       # Missing mandatory keyword argument %r in %s call 
       # missing-kwoa,

       #### E1126 - Sequence index is not an int, slice, or instance with __index__ 
       # Used when a sequence type is indexed with an invalid type. Valid types
       # are ints, slices, and objects with an __index__ method.

       # Sequence index is not an int, slice, or instance with __index__ 
       # invalid-sequence-index,

       #### E1127 - Slice index is not an int, None, or instance with __index__ 
       # Used when a slice index is not an integer, None, or an object with an
       # __index__ method.

       # Slice index is not an int, None, or instance with __index__ 
       # invalid-slice-index,

       #### E1128 - Assigning to function call which only returns None 
       # Used when an assignment is done on a function call but the inferred
       # function returns nothing but None.

       # Assigning to function call which only returns None 
       # assignment-from-none,

       #### E1129 - Context manager '%s' doesn't implement __enter__ and __exit__. 
       # Used when an instance in a with statement doesn't implement the
       # context manager protocol(__enter__/__exit__).

       # Context manager '%s' doesn't implement __enter__ and __exit__. 
       # not-context-manager,

       #### E1130 -  
       # Emitted when a unary operand is used on an object which does not
       # support this type of operation

       #  
       # invalid-unary-operand-type,

       #### E1131 -  
       # Emitted when a binary arithmetic operation between two operands is not
       # supported.

       #  
       # unsupported-binary-operation,

       #### E1132 - Got multiple values for keyword argument %r in function call 
       # Emitted when a function call got multiple values for a keyword.

       # Got multiple values for keyword argument %r in function call 
       # repeated-keyword,

       #### E1133 - Non-iterable value %s is used in an iterating context 
       # Used when a non-iterable value is used in place where iterable is
       # expected

       # Non-iterable value %s is used in an iterating context 
       # not-an-iterable,

       #### E1134 - Non-mapping value %s is used in a mapping context 
       # Used when a non-mapping value is used in place where mapping is
       # expected

       # Non-mapping value %s is used in a mapping context 
       # not-a-mapping,

       #### E1135 - Value '%s' doesn't support membership test 
       # Emitted when an instance in membership test expression doesn't
       # implement membership protocol (__contains__/__iter__/__getitem__)

       # Value '%s' doesn't support membership test 
       # unsupported-membership-test,

       #### E1136 - Value '%s' is unsubscriptable 
       # Emitted when a subscripted value doesn't support subscription(i.e.
       # doesn't define __getitem__ method)

       # Value '%s' is unsubscriptable 
       # unsubscriptable-object,

       #### E1137 - %r does not support item assignment 
       # Emitted when an object does not support item assignment (i.e. doesn't
       # define __setitem__ method)

       # %r does not support item assignment 
       # unsupported-assignment-operation,

       #### E1138 - %r does not support item deletion 
       # Emitted when an object does not support item deletion (i.e. doesn't
       # define __delitem__ method)

       # %r does not support item deletion 
       # unsupported-delete-operation,

       #### E1139 - Invalid metaclass %r used 
       # Emitted whenever we can detect that a class is using, as a metaclass,
       # something which might be invalid for using as a metaclass.

       # Invalid metaclass %r used 
       # invalid-metaclass,

       #### E1200 - Unsupported logging format character %r (%#02x) at index %d 
       # Used when an unsupported format character is used in a logging
       # statement format string.

       # Unsupported logging format character %r (%#02x) at index %d 
       # logging-unsupported-format,

       #### E1201 - Logging format string ends in middle of conversion specifier 
       # Used when a logging statement format string terminates before the end
       # of a conversion specifier.

       # Logging format string ends in middle of conversion specifier 
       # logging-format-truncated,

       #### E1205 - Too many arguments for logging format string 
       # Used when a logging format string is given too many arguments.

       # Too many arguments for logging format string 
       # logging-too-many-args,

       #### E1206 - Not enough arguments for logging format string 
       # Used when a logging format string is given too few arguments.

       # Not enough arguments for logging format string 
       # logging-too-few-args,

       #### E1300 - Unsupported format character %r (%#02x) at index %d 
       # Used when a unsupported format character is used in a format string.

       # Unsupported format character %r (%#02x) at index %d 
       # bad-format-character,

       #### E1301 - Format string ends in middle of conversion specifier 
       # Used when a format string terminates before the end of a conversion
       # specifier.

       # Format string ends in middle of conversion specifier 
       # truncated-format-string,

       #### E1302 - Mixing named and unnamed conversion specifiers in format string 
       # Used when a format string contains both named (e.g. '%(foo)d') and
       # unnamed (e.g. '%d') conversion specifiers. This is also used when a
       # named conversion specifier contains * for the minimum field width
       # and/or precision.

       # Mixing named and unnamed conversion specifiers in format string 
       # mixed-format-string,

       #### E1303 - Expected mapping for format string, not %s 
       # Used when a format string that uses named conversion specifiers is
       # used with an argument that is not a mapping.

       # Expected mapping for format string, not %s 
       # format-needs-mapping,

       #### E1304 - Missing key %r in format string dictionary 
       # Used when a format string that uses named conversion specifiers is
       # used with a dictionary that doesn't contain all the keys required by
       # the format string.

       # Missing key %r in format string dictionary 
       # missing-format-string-key,

       #### E1305 - Too many arguments for format string 
       # Used when a format string that uses unnamed conversion specifiers is
       # given too many arguments.

       # Too many arguments for format string 
       # too-many-format-args,

       #### E1306 - Not enough arguments for format string 
       # Used when a format string that uses unnamed conversion specifiers is
       # given too few arguments

       # Not enough arguments for format string 
       # too-few-format-args,

       #### E1310 - Suspicious argument in %s.%s call 
       # The argument to a str.{l,r,}strip call contains a duplicate character,

       # Suspicious argument in %s.%s call 
       # bad-str-strip-call,

       #### E1700 - Yield inside async function 
       # Used when an `yield` or `yield from` statement is found inside an
       # async function. This message can't be emitted when using Python < 3.5.

       # Yield inside async function 
       # yield-inside-async-function,

       #### E1701 - Async context manager '%s' doesn't implement __aenter__ and __aexit__. 
       # Used when an async context manager is used with an object that does
       # not implement the async context management protocol. This message
       # can't be emitted when using Python < 3.5.

       # Async context manager '%s' doesn't implement __aenter__ and __aexit__. 
       # not-async-context-manager,

       #### F0001 -  
       # Used when an error occurred preventing the analysis of a module
       # (unable to find it for instance).

       #  
       # fatal,

       #### F0002 - %s: %s 
       # Used when an unexpected error occurred while building the Astroid
       # representation. This is usually accompanied by a traceback. Please
       # report such errors !

       # %s: %s 
       # astroid-error,

       #### F0010 - error while code parsing: %s 
       # Used when an exception occurred while building the Astroid
       # representation which could be handled by astroid.

       # error while code parsing: %s 
       # parse-error,

       #### F0202 - Unable to check methods signature (%s / %s) 
       # Used when Pylint has been unable to check methods signature
       # compatibility for an unexpected reason. Please report this kind if you
       # don't make sense of it.

       # Unable to check methods signature (%s / %s) 
       # method-check-failed,

       #### I0001 - Unable to run raw checkers on built-in module %s 
       # Used to inform that a built-in module has not been checked using the
       # raw checkers.

       # Unable to run raw checkers on built-in module %s 
       # raw-checker-failed,

       #### I0010 - Unable to consider inline option %r 
       # Used when an inline option is either badly formatted or can't be used
       # inside modules.

       # Unable to consider inline option %r 
       # bad-inline-option,

       #### I0011 - Locally disabling %s (%s) 
       # Used when an inline option disables a message or a messages category.

       # Locally disabling %s (%s) 
       # locally-disabled,

       #### I0012 - Locally enabling %s (%s) 
       # Used when an inline option enables a message or a messages category.

       # Locally enabling %s (%s) 
       # locally-enabled,

       #### I0013 - Ignoring entire file 
       # Used to inform that the file will not be checked

       # Ignoring entire file 
       # file-ignored,

       #### I0020 - Suppressed %s (from line %d) 
       # A message was triggered on a line, but suppressed explicitly by a
       # disable= comment in the file. This message is not generated for
       # messages that are ignored due to configuration settings.

       # Suppressed %s (from line %d) 
       # suppressed-message,

       #### I0021 - Useless suppression of %s 
       # Reported when a message is explicitly disabled for a line or a block
       # of code, but never triggered.

       # Useless suppression of %s 
       # useless-suppression,

       #### I0022 - Pragma "%s" is deprecated, use "%s" instead 
       # Some inline pylint options have been renamed or reworked, only the
       # most recent form should be used. NOTE:skip-all is only available with
       # pylint >= 0.26

       # Pragma "%s" is deprecated, use "%s" instead 
       # deprecated-pragma,

       #### R0123 - Comparison to literal 
       # Used when comparing an object to a literal, which is usually what you
       # do not want to do, since you can compare to a different literal than
       # what was expected altogether.

       # Comparison to literal 
       # literal-comparison,

       #### R0201 - Method could be a function 
       # Used when a method doesn't use its bound instance, and so could be
       # written as a function.

       # Method could be a function 
       # no-self-use,

       #### R0202 - Consider using a decorator instead of calling classmethod 
       # Used when a class method is defined without using the decorator
       # syntax.

       # Consider using a decorator instead of calling classmethod 
       # no-classmethod-decorator,

       #### R0203 - Consider using a decorator instead of calling staticmethod 
       # Used when a static method is defined without using the decorator
       # syntax.

       # Consider using a decorator instead of calling staticmethod 
       # no-staticmethod-decorator,

       #### R0401 - Cyclic import (%s) 
       # Used when a cyclic import between two or more modules is detected.

       # Cyclic import (%s) 
       # cyclic-import,

       #### R0801 - Similar lines in %s files 
       # Indicates that a set of similar lines has been detected among multiple
       # file. This usually means that the code should be refactored to avoid
       # this duplication.

       # Similar lines in %s files 
       # duplicate-code,

       #### R0901 - Too many ancestors (%s/%s) 
       # Used when class has too many parent classes, try to reduce this to get
       # a simpler (and so easier to use) class.

       # Too many ancestors (%s/%s) 
       # too-many-ancestors,

       #### R0902 - Too many instance attributes (%s/%s) 
       # Used when class has too many instance attributes, try to reduce this
       # to get a simpler (and so easier to use) class.

       # Too many instance attributes (%s/%s) 
       # too-many-instance-attributes,

       #### R0903 - Too few public methods (%s/%s) 
       # Used when class has too few public methods, so be sure it's really
       # worth it.

       # Too few public methods (%s/%s) 
       # too-few-public-methods,

       #### R0904 - Too many public methods (%s/%s) 
       # Used when class has too many public methods, try to reduce this to get
       # a simpler (and so easier to use) class.

       # Too many public methods (%s/%s) 
       # too-many-public-methods,

       #### R0911 - Too many return statements (%s/%s) 
       # Used when a function or method has too many return statement, making
       # it hard to follow.

       # Too many return statements (%s/%s) 
       # too-many-return-statements,

       #### R0912 - Too many branches (%s/%s) 
       # Used when a function or method has too many branches, making it hard
       # to follow.

       # Too many branches (%s/%s) 
       # too-many-branches,

       #### R0913 - Too many arguments (%s/%s) 
       # Used when a function or method takes too many arguments.

       # Too many arguments (%s/%s) 
       # too-many-arguments,

       #### R0914 - Too many local variables (%s/%s) 
       # Used when a function or method has too many local variables.

       # Too many local variables (%s/%s) 
       # too-many-locals,

       #### R0915 - Too many statements (%s/%s) 
       # Used when a function or method has too many statements. You should
       # then split it in smaller functions / methods.

       # Too many statements (%s/%s) 
       # too-many-statements,

       #### R0916 - Too many boolean expressions in if statement (%s/%s) 
       # Used when a if statement contains too many boolean expressions

       # Too many boolean expressions in if statement (%s/%s) 
       # too-many-boolean-expressions,

       #### R1701 - Consider merging these isinstance calls to isinstance(%s, (%s)) 
       # Used when multiple consecutive isinstance calls can be merged into
       # one.

       # Consider merging these isinstance calls to isinstance(%s, (%s)) 
       # consider-merging-isinstance,

       #### R1702 - Too many nested blocks (%s/%s) 
       # Used when a function or a method has too many nested blocks. This
       # makes the code less understandable and maintainable.

       # Too many nested blocks (%s/%s) 
       # too-many-nested-blocks,

       #### R1703 - The if statement can be replaced with %s 
       # Used when an if statement can be replaced with 'bool(test)'.

       # The if statement can be replaced with %s 
       # simplifiable-if-statement,

       #### R1704 - Redefining argument with the local name %r 
       # Used when a local name is redefining an argument, which might suggest
       # a potential error. This is taken in account only for a handful of name
       # binding operations, such as for iteration, with statement assignment
       # and exception handler assignment.

       # Redefining argument with the local name %r 
       # redefined-argument-from-local,

       #### R1705 - Unnecessary "else" after "return" 
       # Used in order to highlight an unnecessary block of code following an
       # if containing a return statement. As such, it will warn when it
       # encounters an else following a chain of ifs, all of them containing a
       # return statement.

       # Unnecessary "else" after "return" 
       # no-else-return,

       #### R1706 - Consider using ternary (%s if %s else %s) 
       # Used when one of known pre-python 2.5 ternary syntax is used.

       # Consider using ternary (%s if %s else %s) 
       # consider-using-ternary,

       #### R1707 - Disallow trailing comma tuple 
       # In Python, a tuple is actually created by the comma symbol, not by the
       # parentheses. Unfortunately, one can actually create a tuple by
       # misplacing a trailing comma, which can lead to potential weird bugs in
       # your code. You should always use parentheses explicitly for creating a
       # tuple. This message can't be emitted when using Python < 3.0.

       # Disallow trailing comma tuple 
       # trailing-comma-tuple,

       #### W0101 - Unreachable code 
       # Used when there is some code behind a "return" or "raise" statement,
       # which will never be accessed.

       # Unreachable code 
       # unreachable,

       #### W0102 - Dangerous default value %s as argument 
       # Used when a mutable value as list or dictionary is detected in a
       # default value for an argument.

       # Dangerous default value %s as argument 
       # dangerous-default-value,

       #### W0104 - Statement seems to have no effect 
       # Used when a statement doesn't have (or at least seems to) any effect.

       # Statement seems to have no effect 
       # pointless-statement,

       #### W0105 - String statement has no effect 
       # Used when a string is used as a statement (which of course has no
       # effect). This is a particular case of W0104 with its own message so
       # you can easily disable it if you're using those strings as
       # documentation, instead of comments.

       # String statement has no effect 
       # pointless-string-statement,

       #### W0106 - Expression "%s" is assigned to nothing 
       # Used when an expression that is not a function call is assigned to
       # nothing. Probably something else was intended.

       # Expression "%s" is assigned to nothing 
       # expression-not-assigned,

       #### W0107 - Unnecessary pass statement 
       # Used when a "pass" statement that can be avoided is encountered.

       # Unnecessary pass statement 
       # unnecessary-pass,

       #### W0108 - Lambda may not be necessary 
       # Used when the body of a lambda expression is a function call on the
       # same argument list as the lambda itself; such lambda expressions are
       # in all but a few cases replaceable with the function being called in
       # the body of the lambda.

       # Lambda may not be necessary 
       # unnecessary-lambda,

       #### W0109 - Duplicate key %r in dictionary 
       # Used when a dictionary expression binds the same key multiple times.

       # Duplicate key %r in dictionary 
       # duplicate-key,

       #### W0111 - Name %s will become a keyword in Python %s 
       # Used when assignment will become invalid in future Python release due
       # to introducing new keyword

       # Name %s will become a keyword in Python %s 
       # assign-to-new-keyword,

       #### W0120 - Else clause on loop without a break statement 
       # Loops should only have an else clause if they can exit early with a
       # break statement, otherwise the statements under else should be on the
       # same scope as the loop itself.

       # Else clause on loop without a break statement 
       # useless-else-on-loop,

       #### W0122 - Use of exec 
       # Used when you use the "exec" statement (function for Python 3), to
       # discourage its usage. That doesn't mean you cannot use it !

       # Use of exec 
       # exec-used,

       #### W0123 - Use of eval 
       # Used when you use the "eval" function, to discourage its usage.
       # Consider using `ast.literal_eval` for safely evaluating strings
       # containing Python expressions from untrusted sources.

       # Use of eval 
       # eval-used,

       #### W0124 - Following "as" with another context manager looks like a tuple. 
       # Emitted when a `with` statement component returns multiple values and
       # uses name binding with `as` only for a part of those values, as in
       # with ctx() as a, b. This can be misleading, since it's not clear if
       # the context manager returns a tuple or if the node without a name
       # binding is another context manager.

       # Following "as" with another context manager looks like a tuple. 
       # confusing-with-statement,

       #### W0125 - Using a conditional statement with a constant value 
       # Emitted when a conditional statement (If or ternary if) uses a
       # constant value for its test. This might not be what the user intended
       # to do.

       # Using a conditional statement with a constant value 
       # using-constant-test,

       #### W0150 - %s statement in finally block may swallow exception 
       # Used when a break or a return statement is found inside the finally
       # clause of a try...finally block: the exceptions raised in the try
       # clause will be silently swallowed instead of being re-raised.

       # %s statement in finally block may swallow exception 
       # lost-exception,

       #### W0199 - Assert called on a 2-uple. Did you mean 'assert x,y'? 
       # A call of assert on a tuple will always evaluate to true if the tuple
       # is not empty, and will always evaluate to false if it is.

       # Assert called on a 2-uple. Did you mean 'assert x,y'? 
       # assert-on-tuple,

       #### W0201 - Attribute %r defined outside __init__ 
       # Used when an instance attribute is defined outside the __init__
       # method.

       # Attribute %r defined outside __init__ 
       # attribute-defined-outside-init,

       #### W0211 - Static method with %r as first argument 
       # Used when a static method has "self" or a value specified in valid-
       # classmethod-first-arg option or valid-metaclass-classmethod-first-arg
       # option as first argument.

       # Static method with %r as first argument 
       # bad-staticmethod-argument,

       #### W0212 - Access to a protected member %s of a client class 
       # Used when a protected member (i.e. class member with a name beginning
       # with an underscore) is access outside the class or a descendant of the
       # class where it's defined.

       # Access to a protected member %s of a client class 
       # protected-access,

       #### W0221 - Parameters differ from %s %r method 
       # Used when a method has a different number of arguments than in the
       # implemented interface or in an overridden method.

       # Parameters differ from %s %r method 
       # arguments-differ,

       #### W0222 - Signature differs from %s %r method 
       # Used when a method signature is different than in the implemented
       # interface or in an overridden method.

       # Signature differs from %s %r method 
       # signature-differs,

       #### W0223 - Method %r is abstract in class %r but is not overridden 
       # Used when an abstract method (i.e. raise NotImplementedError) is not
       # overridden in concrete class.

       # Method %r is abstract in class %r but is not overridden 
       # abstract-method,

       #### W0231 - __init__ method from base class %r is not called 
       # Used when an ancestor class method has an __init__ method which is not
       # called by a derived class.

       # __init__ method from base class %r is not called 
       # super-init-not-called,

       #### W0232 - Class has no __init__ method 
       # Used when a class has no __init__ method, neither its parent classes.

       # Class has no __init__ method 
       # no-init,

       #### W0233 - __init__ method from a non direct base class %r is called 
       # Used when an __init__ method is called on a class which is not in the
       # direct ancestors for the analysed class.

       # __init__ method from a non direct base class %r is called 
       # non-parent-init-called,

       #### W0235 - Useless super delegation in method %r 
       # Used whenever we can detect that an overridden method is useless,
       # relying on super() delegation to do the same thing as another method
       # from the MRO.

       # Useless super delegation in method %r 
       # useless-super-delegation,

       #### W0301 - Unnecessary semicolon 
       # Used when a statement is ended by a semi-colon (";"), which isn't
       # necessary (that's python, not C ;).

       # Unnecessary semicolon 
       # unnecessary-semicolon,

       #### W0311 - Bad indentation. Found %s %s, expected %s 
       # Used when an unexpected number of indentation's tabulations or spaces
       # has been found.

       # Bad indentation. Found %s %s, expected %s 
       # bad-indentation,

       #### W0312 - Found indentation with %ss instead of %ss 
       # Used when there are some mixed tabs and spaces in a module.

       # Found indentation with %ss instead of %ss 
       # mixed-indentation,

       #### W0401 - Wildcard import %s 
       # Used when `from module import *` is detected.

       # Wildcard import %s 
       # wildcard-import,

       #### W0402 - Uses of a deprecated module %r 
       # Used a module marked as deprecated is imported.

       # Uses of a deprecated module %r 
       # deprecated-module,

       #### W0404 - Reimport %r (imported line %s) 
       # Used when a module is reimported multiple times.

       # Reimport %r (imported line %s) 
       # reimported,

       #### W0406 - Module import itself 
       # Used when a module is importing itself.

       # Module import itself 
       # import-self,

       #### W0410 - __future__ import is not the first non docstring statement 
       # Python 2.5 and greater require __future__ import to be the first non
       # docstring statement in the module.

       # __future__ import is not the first non docstring statement 
       # misplaced-future,

       #### W0511 -  
       # Used when a warning note as FIXME or XXX is detected.

       #  
       # fixme,

       #### W0601 - Global variable %r undefined at the module level 
       # Used when a variable is defined through the "global" statement but the
       # variable is not defined in the module scope.

       # Global variable %r undefined at the module level 
       # global-variable-undefined,

       #### W0602 - Using global for %r but no assignment is done 
       # Used when a variable is defined through the "global" statement but no
       # assignment to this variable is done.

       # Using global for %r but no assignment is done 
       # global-variable-not-assigned,

       #### W0603 - Using the global statement 
       # Used when you use the "global" statement to update a global variable.
       # Pylint just try to discourage this usage. That doesn't mean you cannot
       # use it !

       # Using the global statement 
       # global-statement,

       #### W0604 - Using the global statement at the module level 
       # Used when you use the "global" statement at the module level since it
       # has no effect

       # Using the global statement at the module level 
       # global-at-module-level,

       #### W0611 - Unused %s 
       # Used when an imported module or variable is not used.

       # Unused %s 
       # unused-import,

       #### W0612 - Unused variable %r 
       # Used when a variable is defined but not used.

       # Unused variable %r 
       # unused-variable,

       #### W0613 - Unused argument %r 
       # Used when a function or method argument is not used.

       # Unused argument %r 
       # unused-argument,

       #### W0614 - Unused import %s from wildcard import 
       # Used when an imported module or variable is not used from a `'from X
       # import *'` style import.

       # Unused import %s from wildcard import 
       # unused-wildcard-import,

       #### W0621 - Redefining name %r from outer scope (line %s) 
       # Used when a variable's name hide a name defined in the outer scope.

       # Redefining name %r from outer scope (line %s) 
       # redefined-outer-name,

       #### W0622 - Redefining built-in %r 
       # Used when a variable or function override a built-in.

       # Redefining built-in %r 
       # redefined-builtin,

       #### W0623 - Redefining name %r from %s in exception handler 
       # Used when an exception handler assigns the exception to an existing
       # name

       # Redefining name %r from %s in exception handler 
       # redefine-in-handler,

       #### W0631 - Using possibly undefined loop variable %r 
       # Used when an loop variable (i.e. defined by a for loop or a list
       # comprehension or a generator expression) is used outside the loop.

       # Using possibly undefined loop variable %r 
       # undefined-loop-variable,

       #### W0640 - Cell variable %s defined in loop 
       # A variable used in a closure is defined in a loop. This will result in
       # all closures using the same value for the closed-over variable.

       # Cell variable %s defined in loop 
       # cell-var-from-loop,

       #### W0702 - No exception type(s) specified 
       # Used when an except clause doesn't specify exceptions type to catch.

       # No exception type(s) specified 
       # bare-except,

       #### W0703 - Catching too general exception %s 
       # Used when an except catches a too general exception, possibly burying
       # unrelated errors.

       # Catching too general exception %s 
       # broad-except,

       #### W0705 - Catching previously caught exception type %s 
       # Used when an except catches a type that was already caught by a
       # previous handler.

       # Catching previously caught exception type %s 
       # duplicate-except,

       #### W0711 - Exception to catch is the result of a binary "%s" operation 
       # Used when the exception to catch is of the form "except A or B:". If
       # intending to catch multiple, rewrite as "except (A, B):"

       # Exception to catch is the result of a binary "%s" operation 
       # binary-op-exception,

       #### W1201 - Specify string format arguments as logging function parameters 
       # Used when a logging statement has a call form of "logging.<logging
       # method>(format_string % (format_args...))". Such calls should leave
       # string interpolation to the logging method itself and be written
       # "logging.<logging method>(format_string, format_args...)" so that the
       # program may avoid incurring the cost of the interpolation in those
       # cases in which no message will be logged. For more, see
       # http://www.python.org/dev/peps/pep-0282/.

       # Specify string format arguments as logging function parameters 
       # logging-not-lazy,

       #### W1202 - Use % formatting in logging functions and pass the % parameters as arguments 
       # Used when a logging statement has a call form of "logging.<logging
       # method>(format_string.format(format_args...))". Such calls should use
       # % formatting instead, but leave interpolation to the logging function
       # by passing the parameters as arguments.

       # Use % formatting in logging functions and pass the % parameters as arguments 
       # logging-format-interpolation,

       #### W1300 - Format string dictionary key should be a string, not %s 
       # Used when a format string that uses named conversion specifiers is
       # used with a dictionary whose keys are not all strings.

       # Format string dictionary key should be a string, not %s 
       # bad-format-string-key,

       #### W1301 - Unused key %r in format string dictionary 
       # Used when a format string that uses named conversion specifiers is
       # used with a dictionary that contains keys not required by the format
       # string.

       # Unused key %r in format string dictionary 
       # unused-format-string-key,

       #### W1302 - Invalid format string 
       # Used when a PEP 3101 format string is invalid. This message can't be
       # emitted when using Python < 2.7.

       # Invalid format string 
       # bad-format-string,

       #### W1303 - Missing keyword argument %r for format string 
       # Used when a PEP 3101 format string that uses named fields doesn't
       # receive one or more required keywords. This message can't be emitted
       # when using Python < 2.7.

       # Missing keyword argument %r for format string 
       # missing-format-argument-key,

       #### W1304 - Unused format argument %r 
       # Used when a PEP 3101 format string that uses named fields is used with
       # an argument that is not required by the format string. This message
       # can't be emitted when using Python < 2.7.

       # Unused format argument %r 
       # unused-format-string-argument,

       #### W1305 - Format string contains both automatic field numbering and manual field specification 
       # Used when a PEP 3101 format string contains both automatic field
       # numbering (e.g. '{}') and manual field specification (e.g. '{0}').
       # This message can't be emitted when using Python < 2.7.

       # Format string contains both automatic field numbering and manual field specification 
       # format-combined-specification,

       #### W1306 - Missing format attribute %r in format specifier %r 
       # Used when a PEP 3101 format string uses an attribute specifier
       # ({0.length}), but the argument passed for formatting doesn't have that
       # attribute. This message can't be emitted when using Python < 2.7.

       # Missing format attribute %r in format specifier %r 
       # missing-format-attribute,

       #### W1307 - Using invalid lookup key %r in format specifier %r 
       # Used when a PEP 3101 format string uses a lookup specifier ({a[1]}),
       # but the argument passed for formatting doesn't contain or doesn't have
       # that key as an attribute. This message can't be emitted when using
       # Python < 2.7.

       # Using invalid lookup key %r in format specifier %r 
       # invalid-format-index,

       #### W1401 - Anomalous backslash in string: '%s'. String constant might be missing an r prefix. 
       # Used when a backslash is in a literal string but not as an escape.

       # Anomalous backslash in string: '%s'. String constant might be missing an r prefix. 
       # anomalous-backslash-in-string,

       #### W1402 - Anomalous Unicode escape in byte string: '%s'. String constant might be missing an r or u prefix. 
       # Used when an escape like \u is encountered in a byte string where it
       # has no effect.

       # Anomalous Unicode escape in byte string: '%s'. String constant might be missing an r or u prefix. 
       # anomalous-unicode-escape-in-string,

       #### W1501 - "%s" is not a valid mode for open. 
       # Python supports: r, w, a[, x] modes with b, +, and U (only with r)
       # options. See http://docs.python.org/2/library/functions.html#open

       # "%s" is not a valid mode for open. 
       # bad-open-mode,

       #### W1503 - Redundant use of %s with constant value %r 
       # The first argument of assertTrue and assertFalse is a condition. If a
       # constant is passed as parameter, that condition will be always true.
       # In this case a warning should be emitted.

       # Redundant use of %s with constant value %r 
       # redundant-unittest-assert,

       #### W1505 - Using deprecated method %s() 
       # The method is marked as deprecated and will be removed in a future
       # version of Python. Consider looking for an alternative in the
       # documentation.

       # Using deprecated method %s() 
       # deprecated-method,

